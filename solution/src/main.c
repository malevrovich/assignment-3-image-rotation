#include "image.h"

#include <stdio.h>
#include <stdlib.h>

#include "io/image_io.h"
#include "transformations/rotate.h"


int main( int argc, char** argv ) {
    if(argc == 1) printf("Please, choose the source file");
    else if(argc == 2) printf("Please, choose the destionation file");
    else if(argc != 3) printf("Too many arguments!");
    else { 
        struct image src_img;
        
        const enum read_status read_status = read_image_from(argv[1], &src_img);
        if(read_status != 0) {
            fprintf(stderr, "%s\n", read_status_to_str(read_status));
            free(src_img.data);
            return read_status;
        }
        
        const struct image res = rotate(src_img);

        const enum write_status write_status = write_image_to(argv[2], &res);

        free(src_img.data);
        free(res.data);

        if(write_status != 0) {
            fprintf(stderr, "%s\n", write_status_to_str(write_status));
            return write_status;
        }
        return 0;
    }
    return 1;
}

#include "transformations/rotate.h"

#include <stdlib.h>

#include "image.h"

struct image rotate( const struct image src ) {
    struct image res;

    res.data = (struct pixel *) malloc(sizeof(struct pixel) * (size_t ) src.width * (size_t) src.height);
    if(res.data == NULL) return (struct image) {.width = 0, .height = 0, .data = NULL };
    for(int i = 0; i < src.height; i++) {
        for(int j = 0; j < src.width; j++) {
            res.data[j * src.height + src.height - 1 - i] = src.data[i * src.width + j];
        }
    }
    res.width = src.height;
    res.height = src.width;

    return res;
}
